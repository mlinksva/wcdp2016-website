# Website for 2016 Workshop on Collaborative Data Projects

author: [Cheng-Jen Lee](http://about.me/Sollee)

This site is built with a Python Static Site Generator [Pelican](http://getpelican.com).

# Dependencies

* Only tested on Ubuntu 14.04 and Python 3.4 (but it should work in Python 2.7 too).

```
sudo apt-get install build-essential python3-dev
```

* Python dependencies

```
pip3 install -r requirements.txt
```

# Site Generation

```
make publish
```

You can also reference Pelican's [documentation](http://docs.getpelican.com/en/stable/publish.html).
