Title: Sharing of Survey Data in Taiwan
Slug: sharing-of-survey-data-in-taiwan
Lang: en
Date: 2016-11-11
Category: Programme

**Ruoh-Rong Yu**

Research Fellow and Executive Director

Center for Survey Research, Research Center for Humanities and Social Sciences, Academia Sinica, Taiwan

**Abstract**

Founded in November 1994, the Survey Research Data Archive (SRDA) curates the largest collection of digital survey data on social sciences in Taiwan. It engages in the acquisition, preservation, and dissemination of high quality survey data. The SRDA acquires data mostly from academic and governmental agencies. Before release, each dataset goes through a standardized processing procedure to ensure integrity and quality. To get access to the datasets released by the SRDA, a membership is required. The SRDA members include researchers, college teachers, and graduate students, within and outside of Taiwan. Similar to Interuniversity Consortium for Political and Social Research (ICPSR), UK Data Service, Social Science Japan Data Archive (SSJDA), Korea Social Science Data Archive (KOSSDA) and other established survey data centers, SRDA is important in collecting, preserving, and providing access to census and sampling survey data.
