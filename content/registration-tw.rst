報名
####
:slug: registration
:lang: zh-tw
:date: 2016-11-11

您可以在下方表單報名，亦可直接透過 `KKTIX <http://odw.kktix.cc/events/wcdp2016>`_ 報名。

.. raw:: html

   <iframe class="kktix" src="https://kktix.com/tickets_widget?slug=wcdp2016" frameborder="0" height="420" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="auto" allowtransparency="true"></iframe>
