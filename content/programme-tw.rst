議程
####

:slug: programme
:lang: zh-tw
:date: 2016-11-11
:category: Programme

**2016/12/8（四）**

.. list-table::
   :widths: 8 8 18 21
   :header-rows: 1
   :class: table table-striped table-hover

   * - **時間**
     - **議程**
     - **主題**
     - **講者**
   * - 13:30
     - 報  到
     -
     -
   * - 13:50
     - 開場致詞
     -
     - `王新民 <http://www.iis.sinica.edu.tw/pages/whm/index_zh.html>`_ （中央研究院 資訊科學研究所）
   * - 14:00-15:00
     - 議程一，主持人：`王新民 <http://www.iis.sinica.edu.tw/pages/whm/index_zh.html>`_ （中央研究院 資訊科學研究所）
     - 臉書，天上掉下來的禮物：台灣野生動物路死觀察網

       .. container:: medium primary btn icon-right entypo icon-video

          `錄影 <http://m.odw.tw/u/odw/m/odw2016video-01/>`__

       .. container:: medium info btn icon-right entypo icon-newspaper

          `簡報 <http://m.odw.tw/u/odw/m/odw2016slide-01>`__

     - `林德恩 <./speaker.html#te-en-lin>`_ （農委會 台灣特有生物研究保育中心）
   * -
     -
     - Developing a General Framework/Platform for Annotating/Archiving Crowdsourced Multimedia/Knowledge of Biodiversity/Ecology/Environment: Phytophilo Community as an Example

       .. container:: medium primary btn icon-right entypo icon-video

          `錄影 <http://m.odw.tw/u/odw/m/odw2016video-02/>`__

       .. container:: medium info btn icon-right entypo icon-newspaper

          `簡報 <http://m.odw.tw/u/odw/m/odw2016slide-02>`__

     - `王豫煌 <./speaker.html#yu-huang-wang>`_ 、`麥舘碩 <./speaker.html#guan-shuo-mai>`_ （中央研究院 生物多樣性研究中心）
   * -
     -
     - LASS: A Participatory Ecosystem for PM2.5 Monitoring

       .. container:: medium primary btn icon-right entypo icon-video

          `錄影 <http://m.odw.tw/u/odw/m/odw2016video-03/>`__

       .. container:: medium info btn icon-right entypo icon-newspaper

          `簡報 <http://m.odw.tw/u/odw/m/odw2016slide-03>`__

     - `陳伶志 <./speaker.html#ling-jyh-chen>`_ （中央研究院 資訊科學研究所）、`哈爸（許武龍） <./speaker.html#wuulong-hsu>`_ （LASS 創辦人）
   * -
     -
     - 孤兒著作與共同記憶：318 公民運動文物紀錄典藏庫的設置

       .. container:: medium primary btn icon-right entypo icon-video

          `錄影 <http://m.odw.tw/u/odw/m/odw2016video-04/>`__

       .. container:: medium info btn icon-right entypo icon-newspaper

          `簡報 <http://m.odw.tw/u/odw/m/odw2016slide-04>`__

     - `莊庭瑞 <./speaker.html#tyng-ruey-chuang>`_ （中央研究院 資訊科學研究所）
   * - 15:00-15:30
     - 休  息
     -
     -
   * - 15:30-16:30
     - 議程二，主持人：`陳舜伶 <http://www.iias.sinica.edu.tw/content/researcher/contents/2013110516162336964?MSID=2014090310100799433>`_ （中央研究院 法律學研究所）
     - `Sharing of Survey Data in Taiwan <../sharing-of-survey-data-in-taiwan.html>`_

       .. container:: medium primary btn icon-right entypo icon-video

          `錄影 <http://m.odw.tw/u/odw/m/odw2016video-05/>`__

       .. container:: medium info btn icon-right entypo icon-newspaper

          `簡報 <http://m.odw.tw/u/odw/m/odw2016slide-05>`__

     - `于若蓉 <./speaker.html#ruoh-rong-yu>`_ （中央研究院 人文社會科學研究中心 調查研究專題中心） 
   * -
     -
     - Licensing Policies for Collaborative Data Production and Reuse

       .. container:: medium primary btn icon-right entypo icon-video

          `錄影 <http://m.odw.tw/u/odw/m/odw2016video-06/>`__

       .. container:: medium info btn icon-right entypo icon-newspaper

          `簡報 <http://m.odw.tw/u/odw/m/odw2016slide-06>`__

     - `梅娜妮．督儂．德侯內 <./speaker.html#mélanie-dulong-de-rosnay>`_ （法國國家科學研究中心）
   * -
     -
     - `Assessing Value of Biomedical Digital Repositories <../assessing-value-of-biomedical-digital-repositories.html>`_

       .. container:: medium primary btn icon-right entypo icon-video

          `錄影 <http://m.odw.tw/u/odw/m/odw2016video-07/>`__

       .. container:: medium info btn icon-right entypo icon-newspaper

          `簡報 <http://m.odw.tw/u/odw/m/odw2016slide-07>`__

     - `許鈞南 <./speaker.html#chun-nan-hsu>`_ （加州大學聖地牙哥分校醫學院）
   * -
     -
     - `公民科學與資料協作的交會口 <../citizen-science-and-collaborative-data-crosswalks.html>`_

       .. container:: medium primary btn icon-right entypo icon-video

          `錄影 <http://m.odw.tw/u/odw/m/odw2016video-08/>`__

       .. container:: medium info btn icon-right entypo icon-newspaper

          `簡報 <http://m.odw.tw/u/odw/m/odw2016slide-08>`__

     - `麥克．領思維爾 <./speaker.html#mike-linksvayer>`_ （獨立學者）
   * - 16:30-17:00
     - 討  論
     -
     -
