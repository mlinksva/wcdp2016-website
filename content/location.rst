Location
########
:slug: location
:lang: en
:date: 2016-11-11

**Venue: Academia Sinica (AS), Institute of Information Science (IIS), Auditorium 106 at new IIS Building**

**Address: No. 128, Section 2, Academia Road, Nangang, Taipei, Taiwan**

.. raw:: html

   <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.1/dist/leaflet.css" />
   <script src="https://unpkg.com/leaflet@1.0.1/dist/leaflet.js"></script>
   <div id="mapid" style="height: 300px;"></div>
   <script>
     var map = L.map('mapid').setView([25.04114599194039, 121.6146183013916], 18);

     L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw', {
       maxZoom: 18,
       attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
           '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
	   'Imagery © <a href="http://mapbox.com">Mapbox</a>',
       id: 'mapbox.streets'
   }).addTo(map);

     L.marker([25.04114599194039, 121.6146183013916]).addTo(map)
         .bindPopup('IIS, Academia Sinica')
         .openPopup();

   </script>

**AS Campus Map**

.. image:: ./images/sinicamap_e.gif
   :alt: AS Campus Map

The IIS Building is located at "Purple No. 32" on the map.
