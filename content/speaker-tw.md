Title: 講者
Slug: speaker
Lang: zh-tw
Date: 2016-11-11

<a name="te-en-lin"></a>
#### [林德恩] [1]

農委會 台灣特有生物研究保育中心

![Te-En Lin] [p1]

***

<a name="yu-huang-wang"></a>
#### 王豫煌

中央研究院 生物多樣性研究中心

![Yu-Huang Wang] [p2]

***

<a name="guan-shuo-mai"></a>
#### 麥舘碩

中央研究院 生物多樣性研究中心

***

<a name="ling-jyh-chen"></a>
#### [陳伶志] [3]

中央研究院 資訊科學研究所

![Ling-Jyh Chen] [p3]

***

<a name="wuulong-hsu"></a>
#### 哈爸（許武龍）

LASS 創辦人

希望集結台灣 PRO Maker 社群力量，共創讓世人注目的開放硬體計畫，並發展出創新的 Maker 經濟模式。
曾任為晶片設計公司產品線處長，系統架構師，熱愛 Linux，熟悉嵌入式系統設計、通訊系統以及軟體設計。曾任 Computex 國際論壇講者，Linux 認證講師，Java 認證講師。

現正主持LASS開源環境感測網路、擔任幾個社群顧問。

***

<a name="tyng-ruey-chuang"></a>
#### [莊庭瑞] [4]

中央研究院 資訊科學研究所

![Tyng-Ruey Chuang] [p4]

***

<a name="ruoh-rong-yu"></a>
#### [于若蓉] [5]

中央研究院 人文社會科學研究中心 調查研究專題中心

![Ruoh-Rong Yu] [p5]

***

<a name="mélanie-dulong-de-rosnay"></a>
#### [梅娜妮．督儂．德侯內] [6]

法國國家科學研究中心

![Mélanie Dulong de Rosnay] [p6]

***

<a name="chun-nan-hsu"></a>
#### [許鈞南] [7]

加州大學聖地牙哥分校醫學院

![Chun-Nan Hsu] [p7]

***

<a name="mike-linksvayer"></a>
#### [麥克．領思維爾] [8]

獨立學者

![Mike Linksvayer] [p8]

麥克．領思維爾 (Mike Linksvayer) 是「軟體自由保護組織」理事會成員。於 2003 年到 2012 年之間，他服務於 Creative Commons，擔任技術長以及副總裁，目前是資深研究員。早於 2000 年，他共同創立了 Bitzi 這個早期的開放內容∕開放資料的巨型合作平台。

[1]: http://tesri.tesri.gov.tw/research/researcher/z_09.htm
[3]: https://sites.google.com/site/cclljj/
[4]: http://www.iis.sinica.edu.tw/~trc/public/
[5]: http://www.rchss.sinica.edu.tw/people/bio.php?PID=51
[6]: http://www.iscc.cnrs.fr/spip.php?article1558
[7]: https://healthsciences.ucsd.edu/som/dbmi/people/faculty/Pages/chun-nan-hsu.aspx
[8]: http://gondwanaland.com/mlog/

[p1]: ./images/te-en-lin.jpg
[p2]: ./images/yu-huang-wang.jpg
[p3]: ./images/ling-jyh-chen.jpg
[p4]: ./images/tyng-ruey-chuang.jpg
[p5]: ./images/ruoh-rong-yu.jpg
[p6]: ./images/mélanie-dulong-de-rosnay.jpg
[p7]: ./images/chun-nan-hsu.jpg
[p8]: ./images/mike-linksvayer.jpg
