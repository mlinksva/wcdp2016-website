Programme
#########

:slug: programme
:lang: en
:date: 2016-11-11
:category: Programme

**Thursday December 8, 2016**

.. list-table::
   :widths: 8 10 20 17
   :header-rows: 1
   :class: table table-striped table-hover

   * - **Time**
     - **Sessions**
     - **Titles**
     - **Speakers**
   * - 13:30
     - Registration
     -
     -
   * - 13:50
     - Opening Remark
     -
     - `Hsin-Min Wang <http://www.iis.sinica.edu.tw/pages/whm/index_en.html>`_, Institute of Information Science, Academia Sinica
   * - 14:00-15:00
     - Session 1 Moderator: `Hsin-Min Wang <http://www.iis.sinica.edu.tw/pages/whm/index_en.html>`_, Institute of Information Science, Academia Sinica
     - Social Media and Public Engagement: On Taiwan Roadkill Observation Network (TaiRON)

       .. container:: medium primary btn icon-right entypo icon-video

          `Video <http://m.odw.tw/u/odw/m/odw2016video-01/>`__

       .. container:: medium info btn icon-right entypo icon-newspaper

          `Slide <http://m.odw.tw/u/odw/m/odw2016slide-01/>`__

     - `Te-En Lin <./speaker.html#te-en-lin>`_, Taiwan Endemic Species Research Institute
   * -
     -
     - Developing a General Framework/Platform for Annotating/Archiving Crowdsourced Multimedia/Knowledge of Biodiversity/Ecology/Environment: Phytophilo Community as an Example

       .. container:: medium primary btn icon-right entypo icon-video

          `Video <http://m.odw.tw/u/odw/m/odw2016video-02/>`__

       .. container:: medium info btn icon-right entypo icon-newspaper

          `Slide <http://m.odw.tw/u/odw/m/odw2016slide-02>`__

     - `Yu-Huang Wang <./speaker.html#yu-huang-wang>`_ & `Guan-Shuo Mai <./speaker.html#guan-shuo-mai>`_, Research Center for Biodiversity, Academia Sinica
   * -
     -
     - LASS: A Participatory Ecosystem for PM2.5 Monitoring

       .. container:: medium primary btn icon-right entypo icon-video

          `Video <http://m.odw.tw/u/odw/m/odw2016video-03/>`__

       .. container:: medium info btn icon-right entypo icon-newspaper

          `Slide <http://m.odw.tw/u/odw/m/odw2016slide-03>`__

     - `Ling-Jyh Chen <./speaker.html#ling-jyh-chen>`_, Institute of Information Science, Academia Sinica & `Wuulong Hsu <./speaker.html#wuulong-hsu>`_, LASS founder
   * -
     -
     - Orphan Works and Collective Memories: On Setting Up The Sunflower Movement Archive

       .. container:: medium primary btn icon-right entypo icon-video

          `Video <http://m.odw.tw/u/odw/m/odw2016video-04/>`__

       .. container:: medium info btn icon-right entypo icon-newspaper

          `Slide <http://m.odw.tw/u/odw/m/odw2016slide-04>`__

     - `Tyng-Ruey Chuang <./speaker.html#tyng-ruey-chuang>`_, Institute of Information Science, Academia Sinica
   * - 15:00-15:30
     - Break
     -
     -
   * - 15:30-16:30
     - Session 2 Moderator: `Shun-Ling Chen <http://www.iias.sinica.edu.tw/en/content/researcher/contents/2013110517175075138?MSID=2014090310101576713>`_, Institutum Iurisprudentiae, Academia Sinica
     - `Sharing of Survey Data in Taiwan <./sharing-of-survey-data-in-taiwan.html>`_

       .. container:: medium primary btn icon-right entypo icon-video

          `Video <http://m.odw.tw/u/odw/m/odw2016video-05/>`__

       .. container:: medium info btn icon-right entypo icon-newspaper

          `Slide <http://m.odw.tw/u/odw/m/odw2016slide-05>`__

     - `Ruoh-Rong Yu <./speaker.html#ruoh-rong-yu>`_, Center for Survey Research, Research Center for Humanities and Social Sciences, Academia Sinica
   * -
     -
     - Licensing Policies for Collaborative Data Production and Reuse

       .. container:: medium primary btn icon-right entypo icon-video

          `Video <http://m.odw.tw/u/odw/m/odw2016video-06/>`__

       .. container:: medium info btn icon-right entypo icon-newspaper

          `Slide <http://m.odw.tw/u/odw/m/odw2016slide-06>`__

     - `Mélanie Dulong de Rosnay <./speaker.html#mélanie-dulong-de-rosnay>`_, CNRS (Centre National de la Recherche Scientifique)
   * -
     -
     - `Assessing Value of Biomedical Digital Repositories <./assessing-value-of-biomedical-digital-repositories.html>`_

       .. container:: medium primary btn icon-right entypo icon-video

          `Video <http://m.odw.tw/u/odw/m/odw2016video-07/>`__

       .. container:: medium info btn icon-right entypo icon-newspaper

          `Slide <http://m.odw.tw/u/odw/m/odw2016slide-07>`__

     - `Chun-Nan Hsu <./speaker.html#chun-nan-hsu>`_, School of Medicine, UC San Diego
   * -
     -
     - `Citizen Science and Collaborative Data Crosswalks <./citizen-science-and-collaborative-data-crosswalks.html>`_

       .. container:: medium primary btn icon-right entypo icon-video

          `Video <http://m.odw.tw/u/odw/m/odw2016video-08/>`__

       .. container:: medium info btn icon-right entypo icon-newspaper

          `Slide <http://m.odw.tw/u/odw/m/odw2016slide-08>`__

     - `Mike Linksvayer <./speaker.html#mike-linksvayer>`_, Independent Scholar
   * - 16:30-17:00
     - Discussion
     -
     -
