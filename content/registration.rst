Registration
############
:slug: registration
:lang: en
:date: 2016-11-11

You can complete the registration by filling out the form listed below. You can also register directly at `KKTIX <http://odw.kktix.cc/events/wcdp2016>`_.

.. raw:: html

   <iframe class="kktix" src="https://kktix.com/tickets_widget?slug=wcdp2016" frameborder="0" height="420" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="auto" allowtransparency="true"></iframe>
